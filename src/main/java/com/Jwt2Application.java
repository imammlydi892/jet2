package com;

import java.util.ArrayList;

import com.entities.AppUser;
import com.entities.Role;
import com.services.AppUserService;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class Jwt2Application {

	public static void main(String[] args) {
		SpringApplication.run(Jwt2Application.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	// @Bean
	// CommandLineRunner run(AppUserService userService){
	// 	return args ->{
	// 		userService.saveRole(new Role(null,"ROLE_USER"));
	// 		userService.saveRole(new Role(null,"ROLE_MANAGER"));
	// 		userService.saveRole(new Role(null,"ROLE_ADMIN"));
	// 		userService.saveRole(new Role(null,"ROLE_SUPER_ADMIN"));

	// 		userService.saveUser(new AppUser(null,"Imam Mulyadi", "Imam", "123456789", new ArrayList<>()));


	// 		userService.addRoleToUser("Imam", "ROLE_USER");
	// 	};
	// }

}
