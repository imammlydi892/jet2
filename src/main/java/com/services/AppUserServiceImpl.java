package com.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import com.entities.AppUser;
import com.entities.Role;
import com.repo.RoleRepo;
import com.repo.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class AppUserServiceImpl implements AppUserService,UserDetailsService {
    
    // @Autowired
    private final  UserRepo userRepo;

    // @Autowired
    private final RoleRepo roleRepo;


    private final PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        AppUser user = userRepo.findByUserName(username);
        if(user == null){
            log.error("user not found");
            throw new UsernameNotFoundException("user not found");
        }else{
            log.error("user  found: {}", username);
        }
        Collection<SimpleGrantedAuthority> authorities =new ArrayList<>();

        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        
        // UserDetails userDetails = (UserDetails) new User(user.getUserName(),user.getPassword(),authorities);
        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), authorities);
        // return userDetails;
    }
    

    @Override
    public AppUser saveUser(AppUser user) {
        log.info("saving new user {} to the Database", user.getUserName());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepo.save(user); 
    }

    @Override
    public Role saveRole(Role role) {
        log.info("saving new role {} to the Database",role.getName());
        return roleRepo.save(role   );
    }

    @Override
    public void addRoleToUser(String userName, String roleName) {
        log.info("adding role {} to user  {} ",roleName, userName);
        AppUser user = userRepo.findByUserName(userName);
        Role role = roleRepo.findByName(roleName);

        user.getRoles().add(role);
        
    }

    @Override
    public AppUser getUser(String username) {
        log.info("Fetching user {}",username);
        return userRepo.findByUserName(username);
    }

    @Override
    public List<AppUser> getAllUser() {
        log.info("Fetching all user {}");
        return userRepo.findAll();
    }

    
    
}
