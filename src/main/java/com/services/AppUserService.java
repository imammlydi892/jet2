package com.services;

import java.util.List;

import com.entities.AppUser;
import com.entities.Role;



public interface AppUserService {
    
    AppUser saveUser(AppUser user);

    Role saveRole(Role role);

    void addRoleToUser(String userName,String roleName);

    AppUser getUser(String username);

    List<AppUser>getAllUser();
}
